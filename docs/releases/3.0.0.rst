==================
Idem-aiohttp 3.0.0
==================

Idem-aiohttp 3.0.0 bumps its Idem version to Idem 14.
The return state of `idem exec hub.exec.json.*` and `idem exec hub.exec.raw.*` is changed to

.. code::

    {
        # Any return value from this exec module. This is the http response body.
        "ret": Object(),
        # The result of the exec module operation, "True" if the exec module ran successfully, else "False".
        # Previously in Idem-aiohttp 2.*, this field was named as "status".
        "result": True | False,
        # Any comment on the run of this exec module, such as errors.
        "comment": "",
        # Status codes of the http response. Previously in Idem-aiohttp 2.*, this field is named as "status_code".
        "status": "",
        # Headers of the http response.
        "headers": dict
    }
