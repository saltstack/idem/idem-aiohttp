def test_delete(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.delete", url)

    result = ret.json
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


def test_get(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.get", url)

    result = ret.json
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


def test_head(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.head", url)

    result = ret.json["ret"]
    assert "Content-Length" in result
    assert "Content-Type" in result
    assert "Date" in result
    assert "Server" in result


def test_patch(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.patch", url)

    result = ret.json
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


def test_post(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.post", url)

    result = ret.json
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


def test_put(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_json(
        {"foo": "bar"}
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.json.put", url)

    result = ret.json
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True
