def test_delete(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.delete", url)
    assert "foobar" in ret.stdout, ret.stderr


def test_get(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.get", url)
    assert "foobar" in ret.stdout, ret.stderr


def test_head(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.head", url)
    result = ret.json["ret"]
    assert "Content-Length" in result
    assert "Content-Type" in result
    assert "Date" in result
    assert "Server" in result


def test_patch(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.patch", url)
    assert "foobar" in ret.stdout, ret.stderr


def test_post(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.post", url)
    assert "foobar" in ret.stdout, ret.stderr


def test_put(hub, httpserver, idem_cli):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    ret = idem_cli("exec", "request.raw.put", url)
    assert "foobar" in ret.stdout, ret.stderr
