async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="DELETE"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.delete(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}


async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="GET"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.get(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}


async def test_head(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="HEAD"
    ).respond_with_json({})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.head(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["headers"]["Content-Length"] == "2"
    assert ret["headers"]["Content-Type"] == "application/json"
    assert ret["headers"]["Date"]
    assert ret["headers"]["Server"]


async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="PATCH"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.patch(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}


async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="POST"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.post(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}


# Validate content-type is not overridden when specified in the arguments
async def test_post_content_type(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "test"}, method="POST"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    headers = {"content-type": "test"}
    ret = await hub.exec.request.json.post(ctx, url, headers=headers)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}
    assert ret["headers"]["Content-Type"] == "application/json"


async def test_post_content_type(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="POST"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.post(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}


async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request(
        "/foobar", headers={"content-type": "application/json"}, method="PUT"
    ).respond_with_json({"foo": "bar"})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.json.put(ctx, url)

    # check that the request is served
    assert ret["status"], ret.comment
    assert ret["ret"] == {"foo": "bar"}
