async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.raw.delete(ctx, url)

    # check that the request is served
    assert ret["comment"], "OK"
    assert ret["ret"] == b"foobar"
    assert ret["status"], True


async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.raw.get(ctx, url)

    # check that the request is served
    assert ret["comment"], "OK"
    assert ret["ret"] == b"foobar"
    assert ret["status"], True


async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.raw.patch(ctx, url)

    # check that the request is served
    assert ret["comment"], "OK"
    assert ret["ret"] == b"foobar"
    assert ret["status"], True


async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.raw.post(ctx, url)

    # check that the request is served
    assert ret["comment"], "OK"
    assert ret["ret"] == b"foobar"
    assert ret["status"], True


async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.request.raw.put(ctx, url)

    # check that the request is served
    assert ret["comment"], "OK"
    assert ret["ret"] == b"foobar"
    assert ret["status"], True
